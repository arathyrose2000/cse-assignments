Part 1:

USAGE: 
gcc Q1.c -lm; ./a.out input_filename.txt 

In case no input_filename.txt exists, an error is raised. 
The output file path is Assignment/input_filename.txt . 



Part 2:

USAGE:
gcc Q2.c -lm; ./a.out newfile oldfile directory

Here the relative file paths of the oldfile, newfile, and directory should be given. In case, either of the inputs given are incorrect, corresponding error message is displayed.
