/*
 *USAGE:
 * gcc Q2.c -lm; ./a.out newfile oldfile directory
 *
 *Here the relative file paths of the oldfile, newfile, and directory should be given. In case, either of the inputs given are incorrect, corresponding error message is displayed.
 */

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <math.h>

void disp_permissions(__mode_t mode, char *s, int size_s) // Displays the permissions of the file of a mode
{
    //User permissions
    write(1, "\nUser has read permissions on ", 30);
    write(1, s, size_s);
    if (mode & S_IRUSR)
        write(1, ": Yes\n", 7);
    else
        write(1, ": No\n", 6);
    write(1, "User has write permission on ", 30);
    write(1, s, size_s);
    if (mode & S_IWUSR)
        write(1, ": Yes\n", 7);
    else
        write(1, ": No\n", 6);
    write(1, "User has execute permission on ", 32);
    write(1, s, size_s);
    if (mode & S_IXUSR)
        write(1, ": Yes\n", 7);
    else
        write(1, ": No\n", 6);
    //Group permissions
    write(1, "\nGroup has read permissions on ", 32);
    write(1, s, size_s);
    if (mode & S_IRGRP)
        write(1, ": Yes\n", 7);
    else
        write(1, ": No\n", 6);
    write(1, "Group has write permission on ", 30);
    write(1, s, size_s);
    if (mode & S_IWGRP)
        write(1, ": Yes\n", 7);
    else
        write(1, ": No\n", 6);
    write(1, "Group has execute permission on ", 32);
    write(1, s, size_s);
    if (mode & S_IXGRP)
        write(1, ": Yes\n", 7);
    else
        write(1, ": No\n", 6);
    //Others permissions
    write(1, "\nOthers has read permissions on ", 32);
    write(1, s, size_s);
    if (mode & S_IROTH)
        write(1, ": Yes\n", 7);
    else
        write(1, ": No\n", 6);
    write(1, "Others has write permission on ", 32);
    write(1, s, size_s);
    if (mode & S_IWOTH)
        write(1, ": Yes\n", 7);
    else
        write(1, ": No\n", 6);
    write(1, "Others has execute permission on ", 34);
    write(1, s, size_s);
    if (mode & S_IXOTH)
        write(1, ": Yes\n", 7);
    else
        write(1, ": No\n", 6);
}

int main(int argc, char *argv[])
{
    //check whether all items are given
    if (argc != 4)
    {
        write(1, "ERROR: Wrong usage.\nCorrect usage: ./a.out newfile oldfile directory\n", 70); //1 is the terminal
        exit(1);
    }
    //Check whether the directory already exist
    write(1, "Directory is created: ", 23);
    struct stat buffer;
    if (stat(argv[3], &buffer) == 0 && S_ISDIR(buffer.st_mode)) // If directory exists and is a directory
        write(1, "Yes\n", 5);
    else
        write(1, "No\n", 4);
    //Check whether the file contents are reversed in new file
    write(1, "Whether file contents are reversed in newfile: ", 48);
    //open the two files
    int oldfile = open(argv[1], O_RDONLY | __O_LARGEFILE);
    int newfile = open(argv[2], O_RDONLY | __O_LARGEFILE);
    if (oldfile < 0 || newfile < 0) //either 1 of the files do not exist
        write(1, "Files do not exist\n", 20);
    else
    {
        //Check the file length of both the files: if they are not equal then they can not be the reverse of each other
        off_t old_fileLength = lseek(oldfile, 0, SEEK_END);
        off_t new_fileLength = lseek(newfile, 0, SEEK_END);
        // printf("File lengths: %ld %ld\n ", old_fileLength, new_fileLength);
        if (old_fileLength != new_fileLength)
            write(1, "No\n", 4);
        else
        {
            int reversed = 1;
            lseek(oldfile, -1, SEEK_END); //move the old file pointer to the end of file
            lseek(newfile, 0, SEEK_SET);  //move the new file pointer to the beginning of the file
            //set the chunk size: make the chunks 0.1% in size of the whole file
            int pref_chunk_size = old_fileLength / 100;
            int max_chunk_size = (pref_chunk_size == 0) ? 1 : pow(2, (int)(log((double)(pref_chunk_size)) / log(2.0)));
            int start = ((old_fileLength - 1) / max_chunk_size) * max_chunk_size; //Set start to read the remaining end characters of the oldfile
            int length_to_move = old_fileLength - start;                          //i.e. the remaining letters
            char *new_c, *old_c;                                                  //buffers to store the text obtained from the newfile and oldfile respectively
            old_c = (char *)malloc(max_chunk_size);
            new_c = (char *)malloc(max_chunk_size);
            //printf("\nFileLength: %ld \t No of bytes per read: %d \t Start: %d \t Move: %d\n\n", old_fileLength, max_chunk_size, start, length_to_move);
            lseek(oldfile, start, SEEK_SET); //move the old file pointer to start
            while (1)
            {
                //printf("\nGoing to read at %ld %ld for length %d\t:::", lseek(oldfile, 0, SEEK_CUR), lseek(newfile, 0, SEEK_CUR), length_to_move);
                read(oldfile, old_c, length_to_move);
                //old_c[length_to_move] = '\0';
                read(newfile, new_c, length_to_move);
                //new_c[length_to_move] = '\0';
                //printf("Read %s %s\n", old_c, new_c);
                for (int begin = 0, end = length_to_move - 1; begin < length_to_move; begin++, end--)
                    if (new_c[begin] != old_c[end]) //if the buffers are not reverse of each other
                    {
                        reversed = 0;
                        write(1, "No\n", 4);
                        //printf("%c %c",new_c[begin],old_c[end]);
                        break;
                    }
                if (reversed == 0)
                    break;
                if (lseek(oldfile, 0, SEEK_CUR) - max_chunk_size - length_to_move < 0) //if it is not possible to go back, exit the loop
                    break;
                lseek(oldfile, -max_chunk_size - length_to_move, SEEK_CUR);
                length_to_move = max_chunk_size; //after the first iteration, the length to move is same as the chunk size
                //printf("Going back to %ld\t", lseek(oldfile, 0, SEEK_CUR));
            }
            if (reversed == 1)
                write(1, "Yes\n", 5);
        }
    }
    close(oldfile);
    close(newfile);
    //Display the permissions of the newfile
    if (stat(argv[2], &buffer) != 0)
        write(1, "newfile is created: No\n", 24);
    else
        disp_permissions(buffer.st_mode, "newfile", 8);
    //Display the permissions of the oldfile
    if (stat(argv[1], &buffer) != 0)
        write(1, "oldfile is created: No\n", 24);
    else
        disp_permissions(buffer.st_mode, "oldfile", 8);
    //Display the permissions of the directory
    if (stat(argv[3], &buffer) != 0 || !S_ISDIR(buffer.st_mode))
        write(1, "Directory is created: No\n", 26);
    else
        disp_permissions(buffer.st_mode, "directory", 10);
    return 0;
}
