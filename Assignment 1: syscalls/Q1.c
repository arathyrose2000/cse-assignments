/*
 *USAGE: 
 * gcc Q1.c -lm; ./a.out input_filename.txt 
 *
 *In case no input_filename.txt exists, an error is raised. 
 *The output file path is Assignment/input_filename.txt . 
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>
#include <math.h>
#include <string.h>
#include <sys/stat.h>
int main(int argc, char *argv[])
{
    //check the number of arguments
    if (argc != 2)
    {
        write(1, "Invalid usage\nCorrect usage: ./a.out input_filename.txt\n", 91);
        exit(1); //Exit with an error
    }
    int source_file = open(argv[1], O_RDONLY | __O_LARGEFILE);
    if (source_file < 0)
    {
        write(1, "ERROR: The file does not exist\n", 32);
        exit(1);
    }
    off_t fileLength = lseek(source_file, 0, SEEK_END);
    //printf("FILE LENGTH: %ld\n", fileLength);
    if (fileLength <= 0)
    {
        write(1, "ERROR: in reading the file or empty file\n", 42);
        exit(1);
    }
    mkdir("Assignment", 0700);
    char output_path[1000000] = {0}; //make the path of the output file
    strcat(output_path, "Assignment/");
    strcat(output_path, argv[1]);
    int dest_file = open(output_path, O_CREAT | O_RDWR | __O_LARGEFILE | O_TRUNC, 0600);
    if (dest_file < 0) //if it is not possible to make the output file
    {
        write(1, "ERROR: File cannot be created\n", 32);
        exit(1);
    }
    lseek(dest_file, 0, SEEK_SET);          //move it to beginning of file
    lseek(source_file, -1, SEEK_END);       //move it to the end of file
    int pref_byte_size = fileLength / 500; //set the chunk size: make the chunks 0.1% in size of the whole file
    int no_of_bytes_per_read = (pref_byte_size == 0) ? 1 : pow(2, (int)(log((double)(pref_byte_size)) / log(2.0)));
    char *c, *r; //buffers to store the text obtained from the oldfile and reversed versions respectively
    c = (char *)malloc(no_of_bytes_per_read);
    r = (char *)malloc(no_of_bytes_per_read);
    int start = ((fileLength - 1) / no_of_bytes_per_read) * no_of_bytes_per_read; //Set start to read the remaining end characters of the oldfile
    int length_to_move = fileLength - start;                                      //i.e. the remaining letters
    //printf("\nFileLength: %ld \t No of bytes per read: %d \t Start: %d \t Move: %d\n\n", fileLength, no_of_bytes_per_read, start, length_to_move);
    lseek(source_file, start, SEEK_SET); //move the old file pointer to start
    int readbytes = 0;                   //number of bytes read
    char s[100];
    for (int i = 0; i < 100; i++)
        s[i] = ' ';
    while (1)
    {
        readbytes += length_to_move;
        //printf("\nGoing to read at %ld for length %d\t", lseek(source_file, 0, SEEK_CUR), length_to_move);
        read(source_file, c, length_to_move);
        //printf("Read %s\t", c);
        for (int begin = 0, end = length_to_move - 1; begin < length_to_move; begin++, end--)
            r[begin] = c[end];
        r[length_to_move] = '\0';
        //printf("Reversed string: %s\t Writing it there.\t ", r);
        write(dest_file, r, length_to_move);
        char buf[34];
        for (int i = 0; i < 34; i++)
            buf[i] = ' ';
        float percentDone = (float)readbytes / fileLength * 100.0;
        //gcvt(percentDone, 10, buf);
        //write(1, "\r Percentage written: ", 22);
        sprintf(buf, "\rPercentage written: %f", percentDone);
        write(1, buf, 34);
        write(1, "[", 2);
        s[(int)percentDone] = 'O';
        for (int i = 0; i < 100; i++)
            write(1, &s[i], 1);
        write(1, "]", 2);
        fflush(stdout);
        if (lseek(source_file, 0, SEEK_CUR) - no_of_bytes_per_read - length_to_move < 0) //if it is not possible to go back, exit the loop
            break;
        lseek(source_file, -no_of_bytes_per_read - length_to_move, SEEK_CUR);
        //printf("Going back to %ld\t", lseek(source_file, 0, SEEK_CUR));
        length_to_move = no_of_bytes_per_read; //after the first iteration, the length to move is same as the chunk size
    }
    write(1, "\r Percentage written: 100        ", 34);
    write(1, "[", 2);
    for (int i = 0; i < 100; i++)
        write(1, &s[i], 1);
    write(1, "]\n", 3);
    close(source_file);
    close(dest_file);
    return 0;
}
